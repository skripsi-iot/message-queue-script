import logging
import sys
import requests
import json
import time

logging.basicConfig(filename='power.log', format='%(asctime)s - %(levelname)s: %(message)s', level=logging.INFO)

while True:
    response = requests.get('http://localhost/api/B97971DE75/sensors')
    logging.info("[" + str(response.json()['2']['state']) + ", " + str(response.json()['3']['state']) + "]")
    time.sleep(10)
    
