# Message Queue Script

Scripts used for receiving and emit command or response

### Prerequisite
>Install python 3

>Install python pika (menggunakan pip)

>Install python requests (menggunakan pip)

# Emit CMD

Digunakan untuk mengirimkan pesan ke MQ exchange "command" agar selanjutnya pesannya di ambil oleh Gateway bersangkutan untuk melakukan perintah ke Gateway.
Skrip ini akan dijalankan dan dipanggil oleh IoT Cloud Platform.

### Cara menggunakan

Jalankan dengan command:

```
python3 emit_cmd.py  "[HTTP REQUEST METHOD].[GW ID].[API Key].[Path menuju Deconz API yang diinginkan]"
```

Contoh:

```
python3 emit_cmd.py "put.GW1.B97971DE75.lights.1.state"
```

# Recv CMD

Digunakan untuk menerima perintah dari MQ, setelah melanjutkannya ke Deconz Api, skrip akan mengirimkan responnya ke MQ di exchange "response".
Skrip ini akan dijalankan oleh Raspi Gateway selama Gateway menyala

### Cara menggunakan

Jalankan dengan command:

```
python3 emit_cmd.py  "*.[GW ID].#"
```

Contoh:

```
python3 recv_cmd.py "*.GW1.#"
```

# Recv Resp

Digunakan untuk menerima response dari Raspi melalui exchange "response".
Skrip ini akan dijalankan oleh IoT Cloud Platform selama IoT Cloud Platform menyala

### Cara menggunakan
Jalankan dengan command:

```
python3 emit_cmd.py  "[GW ID|*].response"
```

Contoh:

```
python3 recv_resp.py "*.response"
```