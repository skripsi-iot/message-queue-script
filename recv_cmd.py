#!/usr/bin/env python
import pika
import sys
import requests
import json
import logging
import time

GW_ID = "GW1."


logging.basicConfig(filename='/home/message-queue-script/recv_cmd.log', format='%(asctime)s - %(levelname)s: %(message)s', level=logging.INFO)

TIMEOUT_LIMIT = 10

connection = pika.BlockingConnection(
    pika.URLParameters(
        'amqp://feozfqhb:LBNVic1C4ZozR1_e9vWPqijk1Htl0U2X@vulture.rmq.cloudamqp.com/feozfqhb')
)
channel = connection.channel()

channel.exchange_declare(exchange='command', exchange_type='topic')

result = channel.queue_declare(GW_ID)

queue_name = result.method.queue

binding_keys = sys.argv[1:]
if not binding_keys:
    sys.stderr.write("Usage: %s [binding_key]...\n" % sys.argv[0])
    sys.exit(1)

for binding_key in binding_keys:
    channel.queue_bind(
        exchange='command', queue=queue_name, routing_key=binding_key)

logging.info(' [*] Waiting for commands...')

base_path = 'http://localhost/api/'


def callback(ch, method, properties, body):
    logging.info(" [x] %r:%r" % (method.routing_key, body))

    request_arr = method.routing_key.split(".")
    request_method = request_arr[0]
    request_path = "/".join(request_arr[2:])
    request_path = base_path + request_path

    logging.info(" [>] " + request_path)
    logging.info(" [b] " + str(body))
    logging.info("From " + str(properties.timestamp))
    logging.info("Into " + str(time.time()))

    data_response = {}

    if (properties.timestamp != None) and (time.time() - properties.timestamp > TIMEOUT_LIMIT):
        logging.info("Time diff: " + str(time.time() - properties.timestamp))

        data_response = {
            "error":
            {"type": 408,
             "address": request_path,
             "description": "timeout limit reached"}
        }
        data_response = json.dumps(data_response)
    else:
        if request_method == "get":
            r = requests.get(request_path, data=body)
        if request_method == "put":
            r = requests.put(request_path, data=body)
        if request_method == "post":
            r = requests.post(request_path, data=body)
        if request_method == "delete":
            r = requests.delete(request_path, data=body)
        data_response = r.json()

    response_path = ".".join(request_arr[3:])
    response_path = GW_ID + response_path + ".response"
    report_response(response_path, properties, data_response)


def report_response(routing_key, props, message):
    logging.info(" [X] Sending response")

    channel.exchange_declare(exchange='response', exchange_type='topic')
    channel.basic_publish(
        exchange='response',
        routing_key=routing_key,
        properties=pika.BasicProperties(
            correlation_id=props.correlation_id
        ),
        body=json.dumps(message))

    logging.info(" [x] Sent %r:%r" % (routing_key, message))


channel.basic_consume(
    queue=queue_name, on_message_callback=callback, auto_ack=True)

channel.start_consuming()
