#!/usr/bin/env python
import pika
import sys
import json
import time

connection = pika.BlockingConnection(
    pika.URLParameters(
        'amqp://feozfqhb:LBNVic1C4ZozR1_e9vWPqijk1Htl0U2X@vulture.rmq.cloudamqp.com/feozfqhb')
)
channel = connection.channel()

channel.exchange_declare(exchange='command', exchange_type='topic')

routing_key = sys.argv[1] if len(sys.argv) > 1 else 'anonymous.info'

# TO DO Chage data input
print(time.time())
data = { "hue": 1000, "bri": 5 }
message = json.dumps(data)
channel.basic_publish(
    exchange='command',
    routing_key=routing_key,
    body=message,
    properties=pika.BasicProperties(
        delivery_mode=2,
        timestamp=int(time.time())
    ))
print(" [x] Sent %r:%r" % (routing_key, message))
connection.close()
